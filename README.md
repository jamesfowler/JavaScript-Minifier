# NAME

A Javascript port of the Perl library: https://github.com/zoffixznet/JavaScript-Minifier

# SYNOPSIS

To minify a JavaScript file and have the output written directly to another file

    let minifier = require('../lib/JavaScript/Minifier')

    minifier.minify({ input: './js.js', outfile: './js-min.js'})

To minify a JavaScript string literal. Note that by omitting the outfile parameter a the minified code is returned as a string.

    let minifiedJavascript = minifier.minify({ input: 'var x = 2;' })

To include a copyright comment at the top of the minified code.

    minifier.minify({ input: 'var x = 2;', copyright: 'BSD License' })

To treat ';;;' as '//' so that debugging code can be removed. This is a common JavaScript convention for minification.

    minifier.minify({ input: './js.js', stripDebug: 1 })

The "input" parameter is mandatory. The "output", "copyright", and "stripDebug" parameters are optional and can be used in any combination.

# DESCRIPTION

This module removes unnecessary whitespace from JavaScript code. The primary requirement developing this module is to not break working code: if working JavaScript is in input then working JavaScript is output. It is ok if the input has missing semi-colons, snips like '++ +' or '12 .toString()', for example. Internet Explorer conditional comments are copied to the output but the code inside these comments will not be minified.

The ECMAScript specifications allow for many different whitespace characters: space, horizontal tab, vertical tab, new line, carriage return, form feed, and paragraph separator. This module understands all of these as whitespace except for vertical tab and paragraph separator. These two types of whitespace are not minimized.

For static JavaScript files, it is recommended that you minify during the build stage of web deployment. If you minify on-the-fly then it might be a good idea to cache the minified file. Minifying static files on-the-fly repeatedly is wasteful.
