'use strict'

let fs = require('fs')

module.exports = class Minifier {
  static _get(s) {
    if (s.inputType === 'file') {
      if (s.inputPos < s.inputString.length) {
        s.last_read_char = s.inputString.substr(s.inputPos++, 1)
        return s.last_read_char
      } else {
        return undefined;
      }
    } else if (s.inputType === 'string') {
      if (s.inputPos < s.input.length) {
        s.last_read_char = s.input.substr(s.inputPos++, 1)
        return s.last_read_char
      } else {
        return undefined;
      }
    } else {
      throw 'no input'
    }
  }
  static _put(s, x) {
    if (this.defined(s.outfile)) {
      s.outfile_writer.write(x)
    } else {
      s.output = s.output.concat(x)
    }
    return s
  }
  static action1(s) {
    if (!this.isWhitespace(s.a)) s.lastnws = s.a
    s.last = s.a
    s = this.action2(s)
    return s
  }
  static action2(s) {
    s = this._put(s, s.a)
    s = this.action3(s)
    return s
  }
  static action3(s) {
    s.a = s.b
    s = this.action4(s)
    return s
  }
  static action4(s) {
    s.b = s.c
    s.c = s.d
    s.d = this._get(s)
    return s
  }
  static collapseWhitespace(s) {
    while (this.defined(s.a) && this.isWhitespace(s.a) &&
      this.defined(s.b) && this.isWhitespace(s.b)) {
      if (this.isEndspace(s.a) || this.isEndspace(s.b)) {
        s.a = "\n"
      }
      s = this.action4(s)
    }
    return s
  }
  static defined(obj) {
    return typeof obj !== 'undefined'
  }
  static isAlphanum(str) {
    return /[\w\$\\]/.test(str)
  }
  static isEndspace(str) {
    return (str === "\n" || str === "\r" || str === "\f")
  }
  static isInfix(str) {
    return /[,;:=&%*<>\?\|\n]/.test(str)
  }
  static isPostfix(str) {
    return (/[\}\)\]]/.test(str) || this.isInfix(str))
  }
  static isPrefix(str) {
    return (/[\{\(\[!]/.test(str) || this.isInfix(str))
  }
  static isWhitespace(str) {
    // if(str === "\n") console.log('white break')
    return (str === ' ' || str === "\t" || str === "\n" || str === "\r" || str === "\f")
  }
  static minify(args) {
    let h = args
    let s = h
    let ref = s.input

    try {
      let stats = fs.lstatSync(ref);
      if (stats.isFile()) {
        s.inputType = 'file'
        s.inputString = fs.readFileSync(ref, "utf-8");
      }
    } catch (e) {
      s.inputType = 'string'
    }
    s.inputPos = 0;

    if (this.undefined(s.outfile)) {
      s.output = ''
    } else {
      s.outfile_writer = fs.createWriteStream(s.outfile)
    }

    if (s.copyright) {
      s = this._put(s, '/* ' + s.copyright + ' */')
    }

    do {
      s.a = this._get(s)
    } while (this.defined(s.a) && this.isWhitespace(s.a));
    s.b = this._get(s)
    s.c = this._get(s)
    s.d = this._get(s)
    s.last = undefined
    s.lastnws = undefined

    let ccFlag

    while (this.defined(s.a)) {
      if (this.isWhitespace(s.a)) {
        throw 'minifier bug: minify while loop starting with whitespace, stopped'
      }
      if (s.a === '/') {
        if (this.defined(s.b) && s.b === '/') {
          ccFlag = this.defined(s.c) && s.c === '@'
          do {
            ccFlag ? s = this.action2(s) : s = this.action3(s)
          } while (this.defined(s.a) && !this.isEndspace(s.a))
          if (this.defined(s.a)) {
            if (ccFlag) {
              s = this.action1(s)
              s = this.skipWhitespace(s)
            } else if (this.defined(s.last) && !this.isEndspace(s.last) && !this.isPrefix(s.last)) {
              s = this.preserveEndspace(s)
            } else {
              s = this.skipWhitespace(s)
            }
          }
        } else if (this.defined(s.b) && s.b === '*') {
          ccFlag = this.defined(s.c) && s.c === '@'
          do {
            ccFlag ? s = this.action2(s) : s = this.action3(s)
          } while (this.defined(s.b) && !(s.a === '*' && s.b === '/'))
          if (this.defined(s.b)) {
            if (ccFlag) {
              s = this.action2(s)
              s = this.action2(s)
              s = this.preserveEndspace(s)
            } else {
              s = this.action3(s)
              s.a = ' '
              s = this.collapseWhitespace(s)
              if (this.defined(s.last) && this.defined(s.b) &&
                ((this.isAlphanum(s.last) && (this.isAlphanum(s.b || s.b === '.')) ||
                  (s.last === '+' && s.b === '+') || (s.last === '-' && s.b === '-')))) {
                s = this.action1(s)
              } else if (this.defined(s.last) && !this.isPrefix(s.last)) {
                s = this.preserveEndspace(s)
              } else {
                s = this.skipWhitespace(s)
              }
            }
          } else {
            throw 'unterminated comment, stopped'
          }
        } else if (this.defined(s.lastnws) && (s.lastnws === ')' || s.lastnws === ']' ||
            s.lastnws === '.' || this.isAlphanum(s.lastnws))) {
          s = this.action1(s)
          s = this.collapseWhitespace(s)
          this.onWhitespaceConditionalComment(s) ? s = this.action1(s) : s = this.preserveEndspace(s)
        } else {
          s = this.putLiteral(s)
          s = this.collapseWhitespace(s)
          this.onWhitespaceConditionalComment(s) ? s = this.action1(s) : s = this.preserveEndspace(s)
        }
      } else if (s.a === '\'' || s.a === '"') {
        if (this.isEndspace(s.b)) { //look for all "endspace"
          s.b = JSON.stringify(s.b) //stringify adds quotes, is there a more elegant way?
          s.b = s.b.replace(/\"/g, '')
          s = this.putLiteral(s)
        } else if (s.b == '\\' && s.inputType === 'string') { //all other escape chars
          s.b = '\\' + s.b
          s = this.putLiteral(s)
        } else {
          s = this.putLiteral(s)
          s = this.preserveEndspace(s)
        }
      } else if (s.a === '+' || s.a === '-') {
        s = this.action1(s)
        s = this.collapseWhitespace(s)
        if (this.defined(s.a) && this.isWhitespace(s.a)) {
          (this.defined(s.b) && s.b === s.last) ? s = this.action1(s): s = this.preserveEndspace(s)
        }
      } else if (this.isAlphanum(s.a)) {
        s = this.action1(s)
        s = this.collapseWhitespace(s)
        if (this.defined(s.a) && this.isWhitespace(s.a)) {
          (this.defined(s.b) && (this.isAlphanum(s.b) || s.b === '.')) ? s = this.action1(s): s = this.preserveEndspace(s)
        }
      } else if (s.a === ']' || s.a === '}' || s.a === ')') {
        s = this.action1(s)
        s = this.preserveEndspace(s)
      } else if (s.stripDebug && s.a === ';' &&
        this.defined(s.b) && s.b === ';' &&
        this.defined(s.c) && s.c === ';') {
        s = this.action3(s)
        s.a = '/'
        s.b = '/'
      } else {
        s = this.action1(s)
        s = this.skipWhitespace(s)
      }
    }

    if (this.defined(s.last_read_char) && /\n/.test(s.last_read_char)) {
      s = this._put(s, '\n')
    }

    if (this.undefined(s.outfile)) {
      return s.output
    } else {
      return 'Minified output written to ' + s.outfile
    }

  }
  static onWhitespaceConditionalComment(s) {
    return (this.defined(s.a) && this.isWhitespace(s.a) &&
      this.defined(s.b) && s.b === '/' &&
      this.defined(s.c) && (s.c === '/' || s.c === '*') &&
      this.defined(s.d) && s.d === '@')
  }
  static preserveEndspace(s) {
    s = this.collapseWhitespace(s)
    if (this.defined(s.a) && this.isEndspace(s.a) && this.defined(s.b) && !this.isPostfix(s.b)) {
      s = this.action1(s)
    }
    s = this.skipWhitespace(s)
    return s
  }
  static putLiteral(s) {
    let delimiter = s.a
    s = this.action1(s)

    do {
      while (this.defined(s.a) && s.a === '\\') {
        s = this.action1(s)
        s = this.action1(s)
      }
      s = this.action1(s)
    } while (s.last !== delimiter && this.defined(s.a))

    if (s.last !== delimiter) {
      throw 'unterminated ' + (delimiter === '\'' ? 'single quoted string' : delimiter === '"' ? 'double quoted string' : 'reqular expression') + ' literal stopped'
    }

    return s
  }
  static skipWhitespace(s) {
    while (this.defined(s.a) && this.isWhitespace(s.a)) {
      s = this.action3(s)
    }
    return s
  }
  static undefined(obj) {
    return typeof obj === 'undefined'
  }
}
