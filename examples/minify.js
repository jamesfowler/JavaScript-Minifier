'use strict'

let fs = require('fs')

let js = `function setup_sample_form_data() {
    var els, i, l;
    els = $$('.sample_form_data');

    for ( i = 0, l = els.length; i < l; i++ ) {
        els[i].set(
            'data-sample-form-data',
            els[i].get('data-sample-form-data').replace('\\n', "\n")
        );

        if ( els[i].get('value') == '' ) {
            els[i].set('value', els[i].get('data-sample-form-data'));
        }
        else if ( els[i].get('value') != els[i].get('data-sample-form-data') ) {
            els[i].removeClass('sample_form_data');
        }

        var funct_clear_sample_data = function() {
            var i, l, els = this.getElements('.sample_form_data');
            for ( i = 0, l = els.length; i < l; i++ ) {
                els[i].set('value', '');
            }
        }
        els[i].getParent('form').removeEvent('submit', funct_clear_sample_data );
        els[i].getParent('form').addEvent('submit', funct_clear_sample_data );

        els[i].addEvent('focus', function() {
            if ( this.get('value') == this.get('data-sample-form-data') ) {
                this.set('value', '');
                this.removeClass('sample_form_data');
            }
        });

        els[i].addEvent('blur', function() {
            if ( this.value == '' ) {
                this.set('value', this.get('data-sample-form-data'));
                this.addClass('sample_form_data');
            }
        });
    }
}`

let minifier = require('../lib/JavaScript/Minifier')

// console.log(minifier.minify({ input: './js.js', outfile: './js-min.js'}))
// console.log(minifier.minify({ input: 'var x = 2;' }))
// console.log(minifier.minify({ input: 'var x = 2;', copyright: 'BSD License' }))
// console.log(minifier.minify({ input: './js.js', stripDebug: 1 }))
// console.log(minifier.minify({ input: js + '\n' }))
// console.log(minifier.minify({ input: './jquery-2.1.4.js', outfile: './jquery-2.1.4-min.js'}))
// console.log(minifier.minify({ input: './angular.js', outfile: './angular-min.js'}))
console.log(minifier.minify({ input: './react.js', outfile: './react-min.js'}))